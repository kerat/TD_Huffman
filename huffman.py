#!/usr/bin/env python3
# coding: utf8

#####################################################
######  Introduction à la cryptographie  	###
#####   Codes de Huffman             		###
####################################################

from heapq import *
import struct
import argparse

###  distribution de proba sur les letrres
caracteres = [
    ' ', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't',
    'u', 'v', 'w', 'x', 'y', 'z' ]

proba = [
    0.1835, 0.0640, 0.0064, 0.0259, 0.0260, 0.1486, 0.0078,
    0.0083, 0.0061, 0.0591, 0.0023, 0.0001, 0.0465, 0.0245,
    0.0623, 0.0459, 0.0256, 0.0081, 0.0555, 0.0697, 0.0572,
    0.0506, 0.0100, 0.0000, 0.0031, 0.0021, 0.0008  ]

def frequences() :
    table = {}
    n = len(caracteres)
    for i in range(n) :
        table[caracteres[i]] = proba[i]
    return table

def calcule_freq(fichier):
    file = open(fichier, 'r')
    texte = file.read()
    d = {}
    for c in texte:
        if c not in d.keys():
            d[c] = 1
        else:
            d[c] += 1
    # On trie notre dictionnaire par valeur
    list = [(k, d[k]) for k in sorted(d, key=d.get)]
    d = {}
    i = 1
    # On génère un dictionnaire en associant à chaque clef sa position
    # dans la liste.
    for (k, v) in list:
        d[k] = i
        i += 1
    return d

def export_dico(dico, output):
    file = open(output, 'w')
    for k in dico.keys():
        if k == '\n':
            file.write('NAN' + '\n')
        else:
            file.write(k + '\n')
        file.write(dico[k] + '\n')
    file.close()

def import_dico(input):
    dico = {}
    file = open(input, 'r')
    key = ''
    value = ''
    while True:
        key = file.readline()
        if not key: break
        if key == 'NAN\n':
            key = '\n'
            value = file.readline()
            dico[key] = value.replace('\n', '')
        else:
            value = file.readline()
            dico[key.replace('\n', '')] = value.replace('\n', '')
    file.close()
    return dico

###  la classe Arbre
class Arbre :
    def __init__(self, lettre, gauche=None, droit=None):
        self.gauche=gauche
        self.droit=droit
        self.lettre=lettre
    def estFeuille(self):
        return self.gauche == None and self.droit == None
    def estVide(self):
        return self == None
    def __str__(self):
        return '<'+ str(self.lettre)+'.'+str(self.gauche)+'.'+str(self.droit)+'>'
    # On définit une opération == sur l'objet Arbre, ca sera utile pour nos tests
    # unitaires
    def __eq__(self, other):
        if other == None:
            return False
        if self.estFeuille():
            return (self.lettre == other.lettre) and other.estFeuille
        else:
            if self.lettre != other.lettre:
                return False
            if self.gauche == None:
                G=(other.gauche == None)
            else:
                G=(self.gauche == other.gauche)
            if self.droit == None:
                D=(other.droit == None)
            else:
                D=(self.droit == other.droit)
            return G and D
            
###  Ex.1  construction de l'arbre d'Huffman utilisant la structure de "tas binaire"
def arbre_huffman(frequences) :
    h = []
    # On initialise notre tas :
    for k, v in frequences.items():
        heappush(h, (v, k, Arbre(k)))
    # On génère notre arbre :
    while len(h) != 1:
        e1 = heappop(h) # e1 = plus petit élément du tas
        e2 = heappop(h) # e2 = second plus petit élément du tas
        min1, v1, min2, v2 = e1[2], e1[0], e2[2], e2[0] # On récupère respectivement l'arbre et la frequence de chaque élément
        arbre = Arbre('', min1, min2) # On crée un nouvel arbre avec nos deux éléments
        heappush(h, (v1+v2, '', arbre)) # On rajoute notre nouvel arbre au tas
    return h[0][2] # On retourne l'arbre

###  Ex.2  construction du code d'Huffman
def parcours(arbre,prefixe,code) :
    # On effectue un parcours infixe de notre arbre, cad qu'on
    # parcours tout la partie gauche d'un arbre avant de lire sa
    # racine, puis sa partie droite.
    if arbre.gauche != None:
        # Des qu'on va a gauche ou rajoute '0'
        # a notre prefixe
        parcours(arbre.gauche, prefixe+'0', code)
    if arbre.lettre != '':
        # Quand on arrive à une lettre (feuille) on ajoute
        # la lettre et son prefixe au dictionnaire
        code[arbre.lettre]=prefixe
    if arbre.droit != None:
        # Des qu'on va a droite ou rajoute '1'
        # a notre prefixe
        parcours(arbre.droit, prefixe+'1', code)

def code_huffman(arbre) :
    # on remplit le dictionnaire du code d'Huffman en parcourant l'arbre
    code = {}
    parcours(arbre,'',code)
    return code

###  Ex.3  encodage d'un texte contenu dans un fichier
def encodage(dico,fichier):
    file = open(fichier, 'r')
    texte = file.read()
    output = ''
    for c in texte:
        if c not in dico:
            output += dico[' ']
        else:
            output += dico[c]
    file.close()
    return output

def write_to_file(bits_str, file_output):
    # Cette fonction écrit dans file_output, le texte
    # compressé octet par octet (Pas d'écriture bit à bit
    # en python hélas).
    file = open(file_output, 'w+b') # 'b' pour mode 'binaire'
    buffer = 0
    flush = False # Permet de savoir si on va devoir vider notre buffer
    i = 0
    for c in bits_str:
        flush = True
        if c == '0':
            # Décalage à gauche d'un bit
            buffer = buffer<<1
        if c == '1':
            # Décalage à gauche d'un bit
            buffer = buffer<<1
            # Bit de poids faible à 1
            buffer = buffer | 1
        if i==7:
            # Quand le buffer est plein (un octet)
            # on ecrit notre valeur en C style (binaire)
            file.write(struct.pack('<B',buffer)) # '<' = litte endian, 'B' = unsigned char 
            buffer = 0
            i = 0
            flush = False
        else:
            i = i+1
    if flush:
        file.write(struct.pack('<B',buffer))
    file.close()

def read_from_file(file_input):
    i = 0
    output = ''
    file = open(file_input, 'rb')
    bytes = file.read()
    for byte in bytes:
        string = bin(byte).replace('0b','')
        # La fonction bin() retire les 0 inutiles et c'est 
        # très chiant :
        if len(string) < 8 and i != len(bytes)-1 :
            string = ('0'*(8 - len(string))) + string
        output += string
        i += 1
    file.close()
    return output

###  Ex.4  décodage d'un fichier compresse
def decodage(arbre, texte_compresse):
    a = arbre
    output = ''
    # On parcours notre arbre au fur et à mesure qu'on lit
    # le fichier compressé, quand on arrive à une feuille on 
    # ajoute la lettre correspondante au texte decompressé (output)
    # et on reprends notre lecture sans oublier de remonter à la racine
    # de notre arbre.
    for c in texte_compresse:
        if c == '0':
            a = a.gauche
        if c == '1':
            a = a.droit
        if a.estFeuille():
            output += a.lettre
            a = arbre
    return output

def decodage_d(dico, texte_compresse):
    output = ''
    buffer = ''
    for c in texte_compresse:
        buffer += c
        for (k, v) in dico.items():
            if v == buffer:
                output += k
                buffer = ''
    return output

# Juste une meta-procedure de compression
def compress(input, output='out.bin', calc_freq=False, export=None):
    if calc_freq:
        freq = calcule_freq(input)
    else:
        freq = frequences()
    arbre = arbre_huffman(freq)
    dico = code_huffman(arbre)
    if export != None:
        export_dico(dico, export)
    write_to_file(encodage(dico, input), output)

# Juste une meta-procedure de decompression
def decompress(input, output, dico_file=None):
    texte_lu = read_from_file(input)
    if dico_file == None:
        freq = frequences()
        arbre = arbre_huffman(freq)
        decompresse = decodage(arbre, texte_lu)
    else:
        dico = import_dico(dico_file)
        decompresse = decodage_d(dico, texte_lu)
    print(decompresse) 
    if output != None:
        file = open(output, 'w')
        file.write(decompresse)
        file.close()

#def main():
    #freq = frequences()
    #freq = calcule_freq("test_files/horla.txt")
    #print(freq)
    #arbre = arbre_huffman(freq)
    #arbre = Arbre('', Arbre('', Arbre(' '), Arbre('a')), Arbre('', Arbre('c'), Arbre('b')))
    #dico = code_huffman(arbre)
    #export_dico(dico, "dico.dic")
    #texte_compresse = "1101100110011100"
    #print(dico)
    #texte_compresse = encodage(dico, "test_files/horla.txt")
    #print(texte_compresse)
    #write_to_file(texte_compresse, "test_files/texte_compresse.test")
    #texte_lu = read_from_file("test_files/texte_compresse.test")
    #print(texte_lu)
    #print(texte_lu == texte_compresse)
    #print(decodage_d(dico, texte_lu))
    #compress("test_files/horla.txt", "out.bin", True, "dico.dic")
    #decompress("test_files/texte_compresse.test", "horla.txt", "dico.dic")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--decompress", help="Indicate to decompress the file", action="store_true")
    parser.add_argument("--dictionnarie", "--dict", help="Import/Export dictionnarie", type=str)
    parser.add_argument("-o", "--output", help="Output file", type=str)
    parser.add_argument("-f", "--frequencies", help="Indicate to compute frequencies from input in compression", action="store_true")
    parser.add_argument("input", help="The text to compress", type=str)
    args = parser.parse_args()
    output = args.output if args.output else None
    dict  = args.dictionnarie if args.dictionnarie else None
    if args.decompress:
        decompress(args.input, output, dict) 
    else:
        if output == None:
            output = 'out.bin'
        compress(args.input, output, args.frequencies, dict) 
