#!/usr/bin/env python3
# coding: utf8

import unittest
from huffman import *
from filecmp import cmp

class TestHuffman(unittest.TestCase):
    def test_eq_arbre(self):
        arbre_a = Arbre('a', None, None)
        arbre_b = Arbre('b', None, None)
        arbre_a2 = Arbre('a', None, None)
        self.assertEqual(arbre_a, arbre_a2)
        self.assertEqual(arbre_a2, arbre_a)
        self.assertNotEqual(arbre_a, None)
        self.assertNotEqual(arbre_a, arbre_b)
        self.assertNotEqual(arbre_b, arbre_a)
        arbre_taille_2 = Arbre('', arbre_a, None)
        arbre_taille_2_bis = Arbre('', arbre_b, None)
        self.assertNotEqual(arbre_taille_2, arbre_a)
        self.assertNotEqual(arbre_taille_2_bis, arbre_a)
        self.assertNotEqual(arbre_a, arbre_taille_2)
        self.assertNotEqual(arbre_a, arbre_taille_2_bis)
        self.assertNotEqual(arbre_a, None)
        
    def test_arbre_huffman(self):
        frequences = { 'a': 1, 'b': 3, 'c': 2 }
        arbre_oracle = Arbre('', Arbre('', Arbre('a'), Arbre('c')), Arbre('b'))
        arbre = arbre_huffman(frequences)
        self.assertEqual(arbre, arbre_oracle)
        frequences = { 'a': 1, 'b': 3, 'c': 1 }
        arbre_oracle = Arbre('', Arbre('', Arbre('a'), Arbre('c')), Arbre('b'))
        arbre = arbre_huffman(frequences)
        self.assertEqual(arbre, arbre_oracle)

    def test_parcours(self):
        code = {}
        arbre = Arbre('', Arbre('', Arbre('a'), Arbre('c')), Arbre('b'))
        parcours(arbre,'',code)
        code_oracle = {'a':'00', 'b':'1', 'c':'01'}
        self.assertEqual(code_oracle,code)
    
    def test_encodage(self):
        fichier_test = "test_files/test_encodage.txt"
        dico = {'a':'01', 'b':'11', 'c':'10', ' ':'00'}
        texte_encode = encodage(dico, "test_files/test_encodage.txt")
        oracle = "1101100110011100"
        self.assertEqual(texte_encode, oracle)

    def test_write(self):
        bits_str = "1101100110011100" # Hex: D9 9C
        bits_str2 = "11011001110011100" # Hex: D9 CE 00 
        bits_str4 = "1000000000101101" # Hex : 80 2D
        write_to_file(bits_str, "test_files/test_write.test")
        write_to_file(bits_str2, "test_files/test_write2.test")
        write_to_file(bits_str+bits_str2, "test_files/test_write3.test")
        write_to_file(bits_str4, "test_files/test_write4.test")
        self.assertTrue(cmp("test_files/test_write.test", "test_files/test_write.oracle"))
        self.assertTrue(cmp("test_files/test_write2.test", "test_files/test_write2.oracle"))
        self.assertTrue(cmp("test_files/test_write3.test", "test_files/test_write3.oracle"))
        self.assertTrue(cmp("test_files/test_write4.test", "test_files/test_write4.oracle"))

    def test_read(self):
        bits_str = "1101100110011100" # Hex: D9 9C
        bits_str2 = "11011001110011100" # Hex: D9 CE 00 
        bits_str3 = bits_str+bits_str2
        bits_str4 = "1000000000101101" # Hex : 80 2D
        test_read1 = read_from_file("test_files/test_write.oracle")
        test_read2 = read_from_file("test_files/test_write2.oracle")
        test_read3 = read_from_file("test_files/test_write3.oracle")
        test_read4 = read_from_file("test_files/test_write4.oracle")
        self.assertEqual(bits_str, test_read1)
        self.assertEqual(bits_str2, test_read2)
        self.assertEqual(bits_str3, test_read3)
        #self.assertEqual(bits_str4, test_read4)

    def test_decodage(self):
        oracle = "bacacab "
        texte_compresse = "1101100110011100"
        arbre = Arbre('', Arbre('', Arbre(' '), Arbre('a')), Arbre('', Arbre('c'), Arbre('b')))
        test = decodage(arbre, texte_compresse)
        self.assertEqual(oracle, test)
        
    def test_decodage_d(self):
        oracle = "bacacab "
        texte_compresse = "1101100110011100"
        dico = {'a':'01', 'b':'11', 'c':'10', ' ':'00'}
        test = decodage_d(dico, texte_compresse)
        self.assertEqual(oracle, test)

    def test_export_dico(self):
        dico = {'a':'01', 'b':'11', 'c':'10', ' ':'00'}
        export_dico(dico, 'test_files/test_export_dico.test')
        self.assertTrue(cmp("test_files/test_export_dico.test", "test_files/test_export_dico.oracle"))

    def test_import_dico(self):
        oracle = {'a':'01', 'b':'11', 'c':'10', ' ':'00'}
        dico_test = import_dico('test_files/test_export_dico.oracle')
        self.assertEqual(oracle, dico_test)

if __name__ == '__main__':
    unittest.main()
