# Algorithme de compression d'Huffman
Exemple d'utilisation simple :  
 * `./huffman.py -o horla.bin test_files/horla.txt`
 * `./huffman.py -d horla.bin`

Ou alors :
 * `./huffman.py -o horla.bin --dict dico.dic -f test_files/horla.txt`
 * `./huffman.py -d --dict dico.dic horla.bin`

```
usage: huffman.py [-h] [-d] [--dictionnarie DICTIONNARIE] [-o OUTPUT] [-f]
                  input

positional arguments:
  input                 The text to compress

optional arguments:
  -h, --help            show this help message and exit
  -d, --decompress      Indicate to decompress the file
  --dictionnarie DICTIONNARIE, --dict DICTIONNARIE
                        Import/Export dictionnarie
  -o OUTPUT, --output OUTPUT
                        Output file
  -f, --frequencies     Indicate to compute frequencies from input in
                        compression
```
## Tests
Le répertoire test_files/ contient les fichiers de test.  
Pour lancer les tests:   
`python3 test_huffman.py`
